FROM php:7.4-apache

RUN apt-get update && apt-get install -y --no-install-recommends \
	libicu-dev \
	libmagickwand-dev \
	libzip-dev \
    && docker-php-ext-install \
	intl \
	zip \
    && printf "\n" | pecl install imagick \
    && docker-php-ext-enable imagick \
    && rm -rf /var/lib/apt/lists/*

RUN a2enmod rewrite

COPY apache2.conf /etc/apache2/apache2.conf

RUN curl -ksSL https://archivarix.com/download/archivarix.cms.zip | zcat > /var/www/html/archivarix.cms.php \
    && chown -R www-data:www-data /var/www/html
